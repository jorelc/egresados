<?php
namespace Registro;

use Registro\Model\Usuario;
use Registro\Model\UsuarioTable;
use Registro\Model\Programa;
use Registro\Model\ProgramaTable;
use Registro\Model\Facultad;
use Registro\Model\FacultadTable;
use Registro\Model\Empresa;
use Registro\Model\EmpresaTable;
use Registro\Model\Oferta;
use Registro\Model\OfertaTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    // Add this method:
    public function getServiceConfig()
    {
    	return array(
    			'factories' => array(
    					'Registro\Model\UsuarioTable' =>  function($sm) {
    						$tableGateway = $sm->get('UsuarioTableGateway');
    						$table = new UsuarioTable($tableGateway);
    						return $table;
    					},
    					'UsuarioTableGateway' => function ($sm) {
    						$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
    						$resultSetPrototype = new ResultSet();
    						$resultSetPrototype->setArrayObjectPrototype(new Usuario());
    						return new TableGateway('usuarios', $dbAdapter, null, $resultSetPrototype);
    					},
    					'Registro\Model\ProgramaTable' =>  function($sm) {
    						$tableGateway = $sm->get('ProgramaTableGateway');
    						$table = new ProgramaTable($tableGateway);
    						return $table;
    					},
    					'ProgramaTableGateway' => function ($sm) {
    						$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
    						$resultSetPrototype = new ResultSet();
    						$resultSetPrototype->setArrayObjectPrototype(new Programa());
    						return new TableGateway('programas', $dbAdapter, null, $resultSetPrototype);
    					},
    					'Registro\Model\FacultadTable' =>  function($sm) {
    						$tableGateway = $sm->get('FacultadTableGateway');
    						$table = new FacultadTable($tableGateway);
    						return $table;
    					},
    					'FacultadTableGateway' => function ($sm) {
    						$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
    						$resultSetPrototype = new ResultSet();
    						$resultSetPrototype->setArrayObjectPrototype(new Facultad());
    						return new TableGateway('facultades', $dbAdapter, null, $resultSetPrototype);
    					},
    					'Registro\Model\EmpresaTable' =>  function($sm) {
    						$tableGateway = $sm->get('EmpresaTableGateway');
    						$table = new EmpresaTable($tableGateway);
    						return $table;
    					},
    					'EmpresaTableGateway' => function ($sm) {
    						$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
    						$resultSetPrototype = new ResultSet();
    						$resultSetPrototype->setArrayObjectPrototype(new Empresa());
    						return new TableGateway('empresas', $dbAdapter, null, $resultSetPrototype);
    					},
    					'Registro\Model\OfertaTable' =>  function($sm) {
    						$tableGateway = $sm->get('OfertaTableGateway');
    						$table = new OfertaTable($tableGateway);
    						return $table;
    					},
    					'OfertaTableGateway' => function ($sm) {
    						$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
    						$resultSetPrototype = new ResultSet();
    						$resultSetPrototype->setArrayObjectPrototype(new Oferta());
    						return new TableGateway('ofertas', $dbAdapter, null, $resultSetPrototype);
    					},
    			),
    	);
    }
    
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
}