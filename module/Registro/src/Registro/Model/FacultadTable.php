<?php
namespace Registro\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 *
 * @author jorel
 * @version '1.0'
 * @summary Clase Puente entre los datos de la Clase Facultad y el almacenamiento de datos
 */
class FacultadTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function get($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function save(Facultad $facultad)
    {
        $data = array(
            'nombre' => $facultad->nombre,
            'descripcion'  => $facultad->descripcion,
        );

        $id = (int)$facultad->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->get($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    public function delete($id)
    {
        $this->tableGateway->delete(array('id' => $id));
    }
}