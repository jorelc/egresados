<?php
namespace Registro\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 *
 * @author jorel
 * @version '1.0'
 * @summary Clase Puente entre los datos de la Clase Empresa y el almacenamiento de datos
 */
class EmpresaTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function get($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function save(Empresa $empresa)
    {
        $data = array(
            'ciudades_id' => $empresa->ciudades_id,
            'nit'  => $empresa->nit,
            'razon_social'  => $empresa->razon_social,
            'contacto_id'  => $empresa->contacto_id,
            'razon_comercial'  => $empresa->razon_comercial,
            'direccion'  => $empresa->direccion,
            'telefono'  => $empresa->telefono,
        );

        $id = (int)$empresa->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->get($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    public function delete($id)
    {
        $this->tableGateway->delete(array('id' => $id));
    }
}