<?php
namespace Registro\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 *
 * @author jorel
 * @version '1.0'
 * @summary Clase Modelo de entidad Oferta
 */
class Oferta implements InputFilterAwareInterface
{
    public $id;
    public $responsable_id;
    public $empresas_id;
    public $cargo;
    public $salario;
    public $profesion_id;
    public $fecha;
    
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->id     = (!empty($data['id'])) ? $data['id'] : null;
        $this->responsable_id = (!empty($data['responsable_id'])) ? $data['responsable_id'] : null;
        $this->empresas_id  = (!empty($data['empresas_id'])) ? $data['empresas_id'] : null;
        $this->cargo  = (!empty($data['cargo'])) ? $data['cargo'] : null;
        $this->salario  = (!empty($data['salario'])) ? $data['salario'] : null;
        $this->profesion_id  = (!empty($data['profesion_id'])) ? $data['profesion_id'] : null;
        $this->fecha  = (!empty($data['fecha'])) ? $data['fecha'] : null;
    }
    
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
    	throw new \Exception("Not used");
    }
    
    public function getInputFilter()
    {
    	if (!$this->inputFilter) {
    		$inputFilter = new InputFilter();
    		$factory     = new InputFactory();
    
    		$inputFilter->add($factory->createInput(array(
    				'name'     => 'id',
    				'required' => true,
    				'filters'  => array(
    						array('name' => 'Int'),
    				),
    		)));
    
    		$inputFilter->add($factory->createInput(array(
    				'name'     => 'responsable_id',
    				'required' => true,
    				'validators' => array(
    						array(
    								'name' => 'NotEmpty',
    								'options' => array(
    										'messages' => array(
    												\Zend\Validator\NotEmpty::IS_EMPTY => 'El campo es obligatorio',
    										),
    								),
    						),
    		
    				),
    		)));
    		
    		$inputFilter->add($factory->createInput(array(
    				'name'     => 'empresas_id',
    				'required' => true,
    				'validators' => array(
    						array(
    								'name' => 'NotEmpty',
    								'options' => array(
    										'messages' => array(
    												\Zend\Validator\NotEmpty::IS_EMPTY => 'El campo es obligatorio',
    										),
    								),
    						),
    		
    				),
    		)));
    		
    		$inputFilter->add($factory->createInput(array(
    				'name'     => 'cargo',
    				'required' => true,
    				'filters'  => array(
    						array('name' => 'StripTags'),
    						array('name' => 'StringTrim'),
    				),
    				'validators' => array(
							array(
									'name' => 'NotEmpty',
									'options' => array(
											'messages' => array(
													\Zend\Validator\NotEmpty::IS_EMPTY=> 'El campo es obligatorio',
											)
									),
							)
					),
    		)));
    		
    		$inputFilter->add($factory->createInput(array(
    				'name'     => 'salario',
    				'required' => true,
    				'filters'  => array(
    						array('name' => 'StripTags'),
    						array('name' => 'StringTrim'),
    				),
    				'validators' => array(
    						array(
    								'name' => 'NotEmpty',
    								'options' => array(
    										'messages' => array(
    												\Zend\Validator\NotEmpty::IS_EMPTY=> 'El campo es obligatorio',
    										)
    								),
    						)
    				),
    		)));

    		$inputFilter->add($factory->createInput(array(
    				'name'     => 'profesion_id',
    				'required' => true,
    				'validators' => array(
    						array(
    								'name' => 'NotEmpty',
    								'options' => array(
    										'messages' => array(
    												\Zend\Validator\NotEmpty::IS_EMPTY => 'El campo es obligatorio',
    										),
    								),
    						),
    		
    				),
    		)));

    		$inputFilter->add($factory->createInput(array(
    				'name'     => 'fecha',
    				'required' => true,
    				'filters'  => array(
    						array('name' => 'StripTags'),
    						array('name' => 'StringTrim'),
    				),
    				'validators' => array(
    						array(
    								'name' => 'NotEmpty',
    								'options' => array(
    										'messages' => array(
    												\Zend\Validator\NotEmpty::IS_EMPTY => 'El campo es obligatorio',
    										),
    								),
    						),
    				),
    		)));
    		

    		
    		$this->inputFilter = $inputFilter;
    	}
    
    	return $this->inputFilter;
    }
}