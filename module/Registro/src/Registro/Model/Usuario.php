<?php
namespace Registro\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 * 
 * @author jorel
 * @version '1.0'
 * @summary Clase Modelo de entidad Usuario
 */
class Usuario implements InputFilterAwareInterface
{
    public $id;
    public $username;
    public $nombres;
    public $apellidos;
    public $email;
    public $telefono;
    public $celular;
    public $promocion;
    public $ciudades_id;
    public $programas_id;

    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->id     = (!empty($data['id'])) ? $data['id'] : null;
        $this->username = (!empty($data['username'])) ? $data['username'] : null;
        $this->nombres = (!empty($data['nombres'])) ? $data['nombres'] : null;
        $this->apellidos  = (!empty($data['apellidos'])) ? $data['apellidos'] : null;
        $this->email  = (!empty($data['email'])) ? $data['email'] : null;
        $this->telefono  = (!empty($data['telefono'])) ? $data['telefono'] : null;
        $this->celular  = (!empty($data['celular'])) ? $data['celular'] : null;
        $this->promocion  = (!empty($data['promocion'])) ? $data['promocion'] : null;
        $this->ciudades_id  = (!empty($data['ciudades_id'])) ? $data['ciudades_id'] : null;
        $this->programas_id  = (!empty($data['programas_id'])) ? $data['programas_id'] : null;
    }
    
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
    	throw new \Exception("Not used");
    }
    
    public function getInputFilter()
    {
    	if (!$this->inputFilter) {
    		$inputFilter = new InputFilter();
    		$factory     = new InputFactory();
    
    		$inputFilter->add($factory->createInput(array(
    				'name'     => 'id',
    				'required' => true,
    				'filters'  => array(
    						array('name' => 'Int'),
    				),
    		)));
    
    		$inputFilter->add($factory->createInput(array(
    				'name'     => 'username',
    				'required' => true,
    				'filters'  => array(
    						array('name' => 'StripTags'),
    						array('name' => 'StringTrim'),
    				),
    				'validators' => array(
							array(
									'name' => 'regex',
									'options' => array(
											'pattern' => '/^[a-zA-Z ]*$/',
											'messages' => array(
													"regexInvalid"  => 'No utilice caracteres especiales o numeros en este campo',
													"regexNotMatch" => 'No utilice caracteres especiales o numeros en este campo',
													"regexErrorous" => 'No utilice caracteres especiales o numeros en este campo'
											),
									),
							),
							array(
									'name' => 'NotEmpty',
									'options' => array(
											'messages' => array(
													\Zend\Validator\NotEmpty::IS_EMPTY=> 'El campo es obligatorio',
											)
									),
							)
					),
    		)));
    		
    		$inputFilter->add($factory->createInput(array(
    				'name'     => 'nombres',
    				'required' => true,
    				'filters'  => array(
    						array('name' => 'StripTags'),
    						array('name' => 'StringTrim'),
    				),
    				'validators' => array(
    						array(
    								'name' => 'regex',
    								'options' => array(
    										'pattern' => '/^[a-zA-Z ]*$/',
    										'messages' => array(
    												"regexInvalid"  => 'No utilice caracteres especiales o numeros en este campo',
    												"regexNotMatch" => 'No utilice caracteres especiales o numeros en este campo',
    												"regexErrorous" => 'No utilice caracteres especiales o numeros en este campo'
    										),
    								),
    						),
    						array(
    								'name' => 'NotEmpty',
    								'options' => array(
    										'messages' => array(
    												\Zend\Validator\NotEmpty::IS_EMPTY=> 'El campo es obligatorio',
    										)
    								),
    						)
    				),
    		)));

    		$inputFilter->add($factory->createInput(array(
    				'name'     => 'apellidos',
    				'required' => true,
    				'filters'  => array(
    						array('name' => 'StripTags'),
    						array('name' => 'StringTrim'),
    				),
    				'validators' => array(
							array(
									'name' => 'regex',
									'options' => array(
											'pattern' => '/^[a-zA-Z ]*$/',
											'messages' => array(
													"regexInvalid"  => 'No utilice caracteres especiales o numeros en este campo',
													"regexNotMatch" => 'No utilice caracteres especiales o numeros en este campo',
													"regexErrorous" => 'No utilice caracteres especiales o numeros en este campo'
											),
									),
							),
							array(
									'name' => 'NotEmpty',
									'options' => array(
											'messages' => array(
													\Zend\Validator\NotEmpty::IS_EMPTY=> 'El campo es obligatorio',
											)
									),
							)
					),
    		)));
    		
    		$inputFilter->add($factory->createInput(array(
    				'name'     => 'email',
    				'required' => true,
    				'filters'  => array(
    						array('name' => 'StripTags'),
    						array('name' => 'StringTrim'),
    				),
    				'validators' => array(
							array(
									'name' => 'EmailAddress',
									'options' => array(
											'messages' => array(
													\Zend\Validator\EmailAddress::INVALID_FORMAT => 'El formato del correo electronico es invalido'
											)
									)
							),
							array(
									'name' => 'NotEmpty',
									'options' => array(
											'messages' => array(
													\Zend\Validator\NotEmpty::IS_EMPTY=> '',
											)
									),
							),
// 							array(
// 									'name'    => 'Db\NoRecordExists',
// 									'options' => array(
// 											'table' => 'users',
// 											'field' => 'email',
// 											'adapter' => \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter(),
// 											'messages' => array(
// 													\Zend\Validator\Db\NoRecordExists::ERROR_RECORD_FOUND => "Correo Electronico ya registrado"
// 											),
// 									),
// 							),
					),
    		)));
    		
    		$inputFilter->add($factory->createInput(array(
    				'name'     => 'celular',
    				'required' => true,
    				'filters'  => array(
    						array('name' => 'StripTags'),
    						array('name' => 'StringTrim'),
    				),
    				'validators' => array(
							array(
									'name' => 'NotEmpty',
									'options' => array(
											'messages' => array(
													\Zend\Validator\NotEmpty::IS_EMPTY => 'El campo es obligatorio',
											),
									),

							),
							array(
									'name' => 'Digits',
									'options' => array(
											'messages' => array(
													\Zend\Validator\Digits::NOT_DIGITS => 'El campo es obligatorio',
													\Zend\Validator\Digits::STRING_EMPTY => '',

											),
									),
							)
					),
    		)));
    		
    		$inputFilter->add($factory->createInput(array(
    				'name'     => 'telefono',
    				'required' => true,
    				'filters'  => array(
    						array('name' => 'StripTags'),
    						array('name' => 'StringTrim'),
    				),
    				'validators' => array(
    						array(
    								'name' => 'NotEmpty',
    								'options' => array(
    										'messages' => array(
    												\Zend\Validator\NotEmpty::IS_EMPTY => 'El campo es obligatorio',
    										),
    								),
    		
    						),
    						array(
    								'name' => 'Digits',
    								'options' => array(
    										'messages' => array(
    												\Zend\Validator\Digits::NOT_DIGITS => 'El campo es obligatorio',
    												\Zend\Validator\Digits::STRING_EMPTY => '',
    		
    										),
    								),
    						)
    				),
    		)));
    		
    		$inputFilter->add($factory->createInput(array(
    				'name'     => 'promocion',
    				'required' => true,
    				'filters'  => array(
    						array('name' => 'StripTags'),
    						array('name' => 'StringTrim'),
    				),
    				'validators' => array(
							array(
									'name' => 'NotEmpty',
									'options' => array(
											'messages' => array(
													\Zend\Validator\NotEmpty::IS_EMPTY => 'El campo es obligatorio',
											),
									),
							),
					),
    		)));
    		
    		$inputFilter->add($factory->createInput(array(
    				'name'     => 'programas_id',
    				'required' => true,
    				'validators' => array(
    						array(
    								'name' => 'NotEmpty',
    								'options' => array(
    										'messages' => array(
    												\Zend\Validator\NotEmpty::IS_EMPTY => 'El campo es obligatorio',
    										),
    								),
    						),
    				),
    		)));
    
    		$this->inputFilter = $inputFilter;
    	}
    
    	return $this->inputFilter;
    }
}