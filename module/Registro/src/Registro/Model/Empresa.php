<?php
namespace Registro\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 *
 * @author jorel
 * @version '1.0'
 * @summary Clase Modelo de entidad Empresa
 */
class Empresa implements InputFilterAwareInterface
{
    public $id;
    public $ciudades_id;
    public $nit;
    public $razon_social;
    public $contacto_id;
    public $razon_comercial;
    public $direccion;
	public $telefono;
    
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->id     = (!empty($data['id'])) ? $data['id'] : null;
        $this->ciudades_id = (!empty($data['ciudades_id'])) ? $data['ciudades_id'] : null;
        $this->nit  = (!empty($data['nit'])) ? $data['nit'] : null;
        $this->razon_social  = (!empty($data['razon_social'])) ? $data['razon_social'] : null;
        $this->contacto_id  = (!empty($data['contacto_id'])) ? $data['contacto_id'] : null;
        $this->razon_comercial  = (!empty($data['razon_comercial'])) ? $data['razon_comercial'] : null;
        $this->direccion  = (!empty($data['direccion'])) ? $data['direccion'] : null;
        $this->telefono  = (!empty($data['telefono'])) ? $data['telefono'] : null;
    }
    
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
    	throw new \Exception("Not used");
    }
    
    public function getInputFilter()
    {
    	if (!$this->inputFilter) {
    		$inputFilter = new InputFilter();
    		$factory     = new InputFactory();
    
    		$inputFilter->add($factory->createInput(array(
    				'name'     => 'id',
    				'required' => true,
    				'filters'  => array(
    						array('name' => 'Int'),
    				),
    		)));
    
    		$inputFilter->add($factory->createInput(array(
    				'name'     => 'ciudades_id',
    				'required' => true,
    				'validators' => array(
    						array(
    								'name' => 'NotEmpty',
    								'options' => array(
    										'messages' => array(
    												\Zend\Validator\NotEmpty::IS_EMPTY => 'El campo es obligatorio',
    										),
    								),
    						),
    		
    				),
    		)));
    		
    		$inputFilter->add($factory->createInput(array(
    				'name'     => 'nit',
    				'required' => true,
    				'filters'  => array(
    						array('name' => 'StripTags'),
    						array('name' => 'StringTrim'),
    				),
    				'validators' => array(
    						array(
    								'name' => 'NotEmpty',
    								'options' => array(
    										'messages' => array(
    												\Zend\Validator\NotEmpty::IS_EMPTY => 'El campo es obligatorio',
    										),
    								),
    						),
    				),
    		)));
    		
    		$inputFilter->add($factory->createInput(array(
    				'name'     => 'razon_social',
    				'required' => true,
    				'filters'  => array(
    						array('name' => 'StripTags'),
    						array('name' => 'StringTrim'),
    				),
    				'validators' => array(
							array(
									'name' => 'NotEmpty',
									'options' => array(
											'messages' => array(
													\Zend\Validator\NotEmpty::IS_EMPTY=> 'El campo es obligatorio',
											)
									),
							)
					),
    		)));

    		$inputFilter->add($factory->createInput(array(
    				'name'     => 'contacto_id',
    				'required' => true,
    				'validators' => array(
    						array(
    								'name' => 'NotEmpty',
    								'options' => array(
    										'messages' => array(
    												\Zend\Validator\NotEmpty::IS_EMPTY => 'El campo es obligatorio',
    										),
    								),
    						),
    		
    				),
    		)));
    		
    		$inputFilter->add($factory->createInput(array(
    				'name'     => 'razon_comercial',
    				'required' => true,
    				'filters'  => array(
    						array('name' => 'StripTags'),
    						array('name' => 'StringTrim'),
    				),
    				'validators' => array(
							array(
									'name' => 'NotEmpty',
									'options' => array(
											'messages' => array(
													\Zend\Validator\NotEmpty::IS_EMPTY=> 'El campo es obligatorio',
											)
									),
							)
					),
    		)));

    		$inputFilter->add($factory->createInput(array(
    				'name'     => 'direccion',
    				'required' => true,
    				'filters'  => array(
    						array('name' => 'StripTags'),
    						array('name' => 'StringTrim'),
    				),
    				'validators' => array(
    						array(
    								'name' => 'NotEmpty',
    								'options' => array(
    										'messages' => array(
    												\Zend\Validator\NotEmpty::IS_EMPTY => 'El campo es obligatorio',
    										),
    								),
    						),
    				),
    		)));
    		
    		$inputFilter->add($factory->createInput(array(
    				'name'     => 'telefono',
    				'required' => true,
    				'filters'  => array(
    						array('name' => 'StripTags'),
    						array('name' => 'StringTrim'),
    				),
    				'validators' => array(
    						array(
    								'name' => 'NotEmpty',
    								'options' => array(
    										'messages' => array(
    												\Zend\Validator\NotEmpty::IS_EMPTY => 'El campo es obligatorio',
    										),
    								),
    		
    						),
    						array(
    								'name' => 'Digits',
    								'options' => array(
    										'messages' => array(
    												\Zend\Validator\Digits::NOT_DIGITS => 'El campo es obligatorio',
    												\Zend\Validator\Digits::STRING_EMPTY => '',
    		
    										),
    								),
    						)
    				),
    		)));
    		
    		$this->inputFilter = $inputFilter;
    	}
    
    	return $this->inputFilter;
    }
}