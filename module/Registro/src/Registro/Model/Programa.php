<?php
namespace Registro\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 *
 * @author jorel
 * @version '1.0'
 * @summary Clase Modelo de entidad Programa
 */
class Programa implements InputFilterAwareInterface
{
    public $id;
    public $nombre;
    public $descripcion;
    public $facultades_id;

    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->id     = (!empty($data['id'])) ? $data['id'] : null;
        $this->nombre = (!empty($data['nombre'])) ? $data['nombre'] : null;
        $this->descripcion  = (!empty($data['descripcion'])) ? $data['descripcion'] : null;
        $this->facultades_id  = (!empty($data['facultades_id'])) ? $data['facultades_id'] : null;
    }
    
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
    	throw new \Exception("Not used");
    }
    
    public function getInputFilter()
    {
    	if (!$this->inputFilter) {
    		$inputFilter = new InputFilter();
    		$factory     = new InputFactory();
    
    		$inputFilter->add($factory->createInput(array(
    				'name'     => 'id',
    				'required' => true,
    				'filters'  => array(
    						array('name' => 'Int'),
    				),
    		)));
    
    		$inputFilter->add($factory->createInput(array(
    				'name'     => 'nombre',
    				'required' => true,
    				'filters'  => array(
    						array('name' => 'StripTags'),
    						array('name' => 'StringTrim'),
    				),
    				'validators' => array(
							array(
									'name' => 'NotEmpty',
									'options' => array(
											'messages' => array(
													\Zend\Validator\NotEmpty::IS_EMPTY=> 'El campo es obligatorio',
											)
									),
							)
					),
    		)));

    		$inputFilter->add($factory->createInput(array(
    				'name'     => 'descripcion',
    				'required' => true,
    				'filters'  => array(
    						array('name' => 'StripTags'),
    						array('name' => 'StringTrim'),
    				),
    				'validators' => array(
							array(
									'name' => 'NotEmpty',
									'options' => array(
											'messages' => array(
													\Zend\Validator\NotEmpty::IS_EMPTY=> 'El campo es obligatorio',
											)
									),
							)
					),
    		)));
    		
    		$inputFilter->add($factory->createInput(array(
    				'name'     => 'facultades_id',
    				'required' => true,
    				'validators' => array(
    						array(
    								'name' => 'NotEmpty',
    								'options' => array(
    										'messages' => array(
    												\Zend\Validator\NotEmpty::IS_EMPTY => 'El campo es obligatorio',
    										),
    								),
    						),
    		
    				),
    		)));
    		    
    		$this->inputFilter = $inputFilter;
    	}
    
    	return $this->inputFilter;
    }
}