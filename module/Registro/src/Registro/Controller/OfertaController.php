<?php
namespace Registro\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Registro\Model\Oferta;
use Registro\Form\OfertaForm;

/**
 *
 * @author jorel
 * @version '1.0'
 * @summary Clase controladora dentro del patrón MVC para la entidad Oferta
 */
class OfertaController extends AbstractActionController
{
	protected $ofertaTable;
	
    public function indexAction()
    {
    	return new ViewModel(array(
    			'ofertas' => $this->getOfertaTable()->fetchAll(),
    	));
    }

    public function addAction()
    {
    	$usuarioTableGateway = $this->getServiceLocator()->get('Registro\Model\UsuarioTable');
    	$empresaTableGateway = $this->getServiceLocator()->get('Registro\Model\EmpresaTable');
    	$programaTableGateway = $this->getServiceLocator()->get('Registro\Model\ProgramaTable');
    	 
    	$form = new OfertaForm($usuarioTableGateway, $empresaTableGateway, $programaTableGateway);
    	$form->get('submit')->setValue('Agregar');
    	
    	$form->get('fecha')->setValue(date('Y-m-d H:i:s', time()));
    	 
    	$request = $this->getRequest();
    	if ($request->isPost()) {
    		$oferta = new Oferta();
    		$form->setInputFilter($oferta->getInputFilter());
    		$form->setData($request->getPost());
    	
    		if ($form->isValid()) {
    			$oferta->exchangeArray($form->getData());
    			$this->getOfertaTable()->save($oferta);
    	
    			return $this->redirect()->toRoute('oferta');
    		}
    	}
    	return array('form' => $form);
    }

    public function editAction()
    {
    }

    public function deleteAction()
    {
    }
    
    public function getOfertaTable()
    {
    	if (!$this->ofertaTable) {
    		$sm = $this->getServiceLocator();
    		$this->ofertaTable = $sm->get('Registro\Model\OfertaTable');
    	}
    	return $this->ofertaTable;
    }
}