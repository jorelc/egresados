<?php
namespace Registro\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Registro\Model\Usuario;
use Registro\Form\UsuarioForm;

/**
 *
 * @author jorel
 * @version '1.0'
 * @summary Clase controladora dentro del patrón MVC para la entidad Usuario
 */
class UsuarioController extends AbstractActionController
{
	protected $usuarioTable;
	
    public function indexAction()
    {
    	return new ViewModel(array(
    			'usuarios' => $this->getUsuarioTable()->fetchAll(),
    	));
    }

    public function addAction()
    {
    	$programaTableGateway = $this->getServiceLocator()->get('Registro\Model\ProgramaTable');
    	
    	$form = new UsuarioForm($programaTableGateway);
    	$form->get('submit')->setValue('Agregar');
    	
    	$form->get('ciudades_id')->setValue('1');
    	 
    	$request = $this->getRequest();
    	if ($request->isPost()) {
    		$usuario = new Usuario();
    		$form->setInputFilter($usuario->getInputFilter());
    		$form->setData($request->getPost());
    	
    		if ($form->isValid()) {
    			$usuario->exchangeArray($form->getData());
    			$this->getUsuarioTable()->save($usuario);
    	
    			return $this->redirect()->toRoute('usuario');
    		}
    	}
    	return array('form' => $form);
    }

    public function editAction()
    {
    }

    public function deleteAction()
    {
    }
    
    public function getUsuarioTable()
    {
    	if (!$this->usuarioTable) {
    		$sm = $this->getServiceLocator();
    		$this->usuarioTable = $sm->get('Registro\Model\UsuarioTable');
    	}
    	return $this->usuarioTable;
    }
}