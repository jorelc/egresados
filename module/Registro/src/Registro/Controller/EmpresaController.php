<?php
namespace Registro\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Registro\Model\Empresa;
use Registro\Form\EmpresaForm;

/**
 *
 * @author jorel
 * @version '1.0'
 * @summary Clase controladora dentro del patrón MVC para la entidad Empresa
 */
class EmpresaController extends AbstractActionController
{
	protected $empresaTable;
	
    public function indexAction()
    {
    	return new ViewModel(array(
    			'empresas' => $this->getEmpresaTable()->fetchAll(),
    	));
    }

    public function addAction()
    {
    	$usuarioTableGateway = $this->getServiceLocator()->get('Registro\Model\UsuarioTable');
    	
    	$form = new EmpresaForm($usuarioTableGateway);
    	$form->get('submit')->setValue('Agregar');
    	
    	$form->get('ciudades_id')->setValue('1');
    	 
    	$request = $this->getRequest();
    	if ($request->isPost()) {
    		$empresa = new Empresa();
    		$form->setInputFilter($empresa->getInputFilter());
    		$form->setData($request->getPost());
    	
    		if ($form->isValid()) {
    			$empresa->exchangeArray($form->getData());
    			$this->getEmpresaTable()->save($empresa);
    	
    			return $this->redirect()->toRoute('empresa');
    		}
    	}
    	return array('form' => $form);
    }

    public function editAction()
    {
    }

    public function deleteAction()
    {
    }
    
    public function getEmpresaTable()
    {
    	if (!$this->empresaTable) {
    		$sm = $this->getServiceLocator();
    		$this->empresaTable = $sm->get('Registro\Model\EmpresaTable');
    	}
    	return $this->empresaTable;
    }
}