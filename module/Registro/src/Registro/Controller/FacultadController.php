<?php
namespace Registro\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Registro\Model\Facultad;
use Registro\Form\FacultadForm;

/**
 *
 * @author jorel
 * @version '1.0'
 * @summary Clase controladora dentro del patrón MVC para la entidad Facultad
 */
class FacultadController extends AbstractActionController
{
	protected $facultadTable;
	
    public function indexAction()
    {
    	return new ViewModel(array(
    			'facultades' => $this->getFacultadTable()->fetchAll(),
    	));
    }

    public function addAction()
    {
    	$form = new FacultadForm();
    	$form->get('submit')->setValue('Agregar');
    	
    	$request = $this->getRequest();
    	if ($request->isPost()) {
    		$facultad = new Facultad();
    		$form->setInputFilter($facultad->getInputFilter());
    		$form->setData($request->getPost());
    	
    		if ($form->isValid()) {
    			$facultad->exchangeArray($form->getData());
    			$this->getFacultadTable()->save($facultad);
    	
    			return $this->redirect()->toRoute('facultad');
    		}
    	}
    	return array('form' => $form);
    }

    public function editAction()
    {
    }

    public function deleteAction()
    {
    }
    
    public function getFacultadTable()
    {
    	if (!$this->facultadTable) {
    		$sm = $this->getServiceLocator();
    		$this->facultadTable = $sm->get('Registro\Model\FacultadTable');
    	}
    	return $this->facultadTable;
    }
}