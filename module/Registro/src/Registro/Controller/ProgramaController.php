<?php
namespace Registro\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Registro\Model\Programa;
use Registro\Form\ProgramaForm;

/**
 *
 * @author jorel
 * @version '1.0'
 * @summary Clase controladora dentro del patrón MVC para la entidad Programa
 */
class ProgramaController extends AbstractActionController
{
	protected $programaTable;
	
    public function indexAction()
    {
    	return new ViewModel(array(
    			'programas' => $this->getProgramaTable()->fetchAll(),
    	));
    }

    public function addAction()
    {
    	$facultadTableGateway = $this->getServiceLocator()->get('Registro\Model\FacultadTable');
    	
    	$form = new ProgramaForm($facultadTableGateway);
    	$form->get('submit')->setValue('Agregar');
    	
    	$form->get('facultades_id')->setValue('1');
    	 
    	$request = $this->getRequest();
    	if ($request->isPost()) {
    		$programa = new Programa();
    		$form->setInputFilter($programa->getInputFilter());
    		$form->setData($request->getPost());
    	
    		if ($form->isValid()) {
    			$programa->exchangeArray($form->getData());
    			$this->getProgramaTable()->save($programa);
    	
    			return $this->redirect()->toRoute('programa');
    		}
    	}
    	return array('form' => $form);
    }

    public function editAction()
    {
    }

    public function deleteAction()
    {
    }
    
    public function getProgramaTable()
    {
    	if (!$this->programaTable) {
    		$sm = $this->getServiceLocator();
    		$this->programaTable = $sm->get('Registro\Model\ProgramaTable');
    	}
    	return $this->programaTable;
    }
}