<?php
namespace Registro\Form;

use Zend\Form\Form;
use Registro\Model\UsuarioTable;
use Registro\Model\EmpresaTable;
use Registro\Model\ProgramaTable;

class OfertaForm extends Form
{
	protected $usuarioTable;
	protected $empresaTable;
	protected $programaTable;
	
    public function __construct(UsuarioTable $usuarioTable, EmpresaTable $empresaTable, ProgramaTable $programaTable)
    {
        parent::__construct('oferta');
        
        $this->setUsuarioTable($usuarioTable);
        $this->setEmpresaTable($empresaTable);
        $this->setProgramaTable($programaTable);
        
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');
        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));
        
        $this->add(array(
        		'type' => 'Select',
        		'name' => 'responsable_id',
        		'options' => array(
        				'label' => 'Responsable:',
        				'disable_inarray_validator' => false,
        				'value_options' => $this->getUsuarios(),
        		)
        ));
        
        $this->add(array(
        		'type' => 'Select',
        		'name' => 'empresas_id',
        		'options' => array(
        				'label' => 'Empresa:',
        				'disable_inarray_validator' => false,
        				'value_options' => $this->getEmpresas(),
        		)
        ));
        
        $this->add(array(
            'name' => 'cargo',
            'type' => 'Text',
            'options' => array(
                'label' => 'Cargo',
            ),
        ));
 
        $this->add(array(
        		'name' => 'salario',
        		'type' => 'Text',
        		'options' => array(
        				'label' => 'Salario',
        		),
        ));
        
        $this->add(array(
        		'type' => 'Select',
        		'name' => 'profesion_id',
        		'options' => array(
        				'label' => 'Profesion:',
        				'disable_inarray_validator' => false,
        				'value_options' => $this->getProgramas(),
        		)
        ));
        
        $this->add(array(
        		'name' => 'fecha',
        		'type' => 'Hidden',
        		'options' => array(
        				'value' => date('Y-m-d H:i:s', time()),
        		),
        ));
        
        $this->add(array(
        		'type' => 'Zend\Form\Element\Csrf',
        		'name' => 'csrf',
        		'options' => array(
        				'csrf_options' => array(
        						'timeout' => 600
        				)
        		)
        ));
        
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Enviar',
            	'class' => 'btn',	
                'id' => 'submitbutton',
            ),
        ));
    }
    
    public function getUsuarios()
    {
    	$table = $this->getUsuarioTable();
    	$data  = $table->fetchAll();
    	$selectData = array();
    
    	foreach ($data as $selectOption) {
    		$selectData[$selectOption->id] = $selectOption->nombres . " " . $selectOption->apellidos;
    	}
    	return $selectData;
    }
    
    public function setUsuarioTable($usuarioTable)
    {
    	$this->usuarioTable = $usuarioTable;
    	return $this;
    }
    
    public function getUsuarioTable()
    {
    	return $this->usuarioTable;
    }
    
    public function getEmpresas()
    {
    	$table = $this->getEmpresaTable();
    	$data  = $table->fetchAll();
    	$selectData = array();
    
    	foreach ($data as $selectOption) {
    		$selectData[$selectOption->id] = $selectOption->razon_social;
    	}
    	return $selectData;
    }
    
    public function setEmpresaTable($empresaTable)
    {
    	$this->empresaTable = $empresaTable;
    	return $this;
    }
    
    public function getEmpresaTable()
    {
    	return $this->empresaTable;
    }
    
    public function getProgramas()
    {
    	$table = $this->getProgramaTable();
    	$data = $table->fetchAll();
    	$selectData = array();
    	 
    	foreach ($data as $selectOption) {
    		$selectData[$selectOption->id] = $selectOption->nombre;
    	}
    	return $selectData;
    }
    
    public function setProgramaTable($programaTable)
    {
    	$this->programaTable = $programaTable;
    	return $this;
    }
    
    public function getProgramaTable()
    {
    	return $this->programaTable;
    }
}