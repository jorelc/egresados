<?php
namespace Registro\Form;

use Zend\Form\Form;
use Registro\Model\ProgramaTable;

class UsuarioForm extends Form
{
	protected $programaTable;
	
    public function __construct(ProgramaTable $programaTable)
    {
        $this->setProgramaTable($programaTable);
        
    	parent::__construct('usuario');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');
        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));
        
        $this->add(array(
            'name' => 'username',
            'type' => 'Text',
            'options' => array(
                'label' => 'Usuario',
            ),
        ));
        
        $this->add(array(
            'name' => 'nombres',
            'type' => 'Text',
            'options' => array(
                'label' => 'Nombres',
            ),
        ));
        
        $this->add(array(
            'name' => 'apellidos',
            'type' => 'Text',
            'options' => array(
                'label' => 'Apellidos',
            ),
        ));
        $this->add(array(
            'name' => 'email',
            'type' => 'Text',
            'options' => array(
                'label' => 'email',
            ),
        ));
        $this->add(array(
            'name' => 'telefono',
            'type' => 'Text',
            'options' => array(
                'label' => 'Telefono',
            ),
        ));
        $this->add(array(
            'name' => 'celular',
            'type' => 'Text',
            'options' => array(
                'label' => 'Celular',
            ),
        ));
        $this->add(array(
            'name' => 'promocion',
            'type' => 'Text',
            'options' => array(
                'label' => 'Promocion',
            ),
        ));
        
        $this->add(array(
            'name' => 'ciudades_id',
            'type' => 'Hidden',
        	'options' => array(
        		'value' => '1',
        	),
        ));
        
        $this->add(array(
        		'type' => 'Select',
        		'name' => 'programas_id',
        		'options' => array(
        				'label' => 'Programa:',
        				'disable_inarray_validator' => false,
        				'value_options' => $this->getProgramas(),
        		)
        ));
        
        $this->add(array(
        		'type' => 'Zend\Form\Element\Csrf',
        		'name' => 'csrf',
        		'options' => array(
        				'csrf_options' => array(
        						'timeout' => 600
        				)
        		)
        ));
        
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Enviar',
            	'class' => 'btn',	
                'id' => 'submitbutton',
            ),
        ));
    }
    
    public function getProgramas()
    {
    	$table = $this->getProgramaTable();
    	$data = $table->fetchAll();
    	$selectData = array();
    	
    	foreach ($data as $selectOption) {
    		$selectData[$selectOption->id] = $selectOption->nombre;
    	}
    	return $selectData;
    }
    
    public function setProgramaTable($programaTable)
    {
    	$this->programaTable = $programaTable;
    	return $this;
    }
    
    public function getProgramaTable()
    {
    	return $this->programaTable;
    }
}