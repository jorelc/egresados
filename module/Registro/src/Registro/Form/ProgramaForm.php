<?php
namespace Registro\Form;

use Zend\Form\Form;
use Registro\Model\FacultadTable;

class ProgramaForm extends Form
{
	protected $facultadTable;
	
    public function __construct(FacultadTable $facultadTable)
    {
        // we want to ignore the name passed
        parent::__construct('programa');
        
        $this->setFacultadTable($facultadTable);
        
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');
        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'name' => 'nombre',
            'type' => 'Text',
            'options' => array(
                'label' => 'Nombre',
            ),
        ));
        $this->add(array(
            'name' => 'descripcion',
            'type' => 'Text',
            'options' => array(
                'label' => 'Descripcion',
            ),
        ));
 
        $this->add(array(
        		'type' => 'Select',
        		'name' => 'facultades_id',
        		'options' => array(
        				'label' => 'Facultad:',
        				'disable_inarray_validator' => false,
        				'value_options' => $this->getFacultades(),
        		)
        ));
        
        $this->add(array(
        		'type' => 'Zend\Form\Element\Csrf',
        		'name' => 'csrf',
        		'options' => array(
        				'csrf_options' => array(
        						'timeout' => 600
        				)
        		)
        ));
        
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Enviar',
            	'class' => 'btn',	
                'id' => 'submitbutton',
            ),
        ));
    }
    
    public function getFacultades()
    {
    	$table = $this->getFacultadTable();
    	$data  = $table->fetchAll();
    	$selectData = array();
    
    	foreach ($data as $selectOption) {
    		$selectData[$selectOption->id] = $selectOption->nombre;
    	}
    	return $selectData;
    }
    
    public function setFacultadTable($facultadTable)
    {
    	$this->facultadTable = $facultadTable;
    	return $this;
    }
    
    public function getFacultadTable()
    {
    	return $this->facultadTable;
    }
}