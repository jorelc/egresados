<?php
namespace Registro\Form;

use Zend\Form\Form;
use Registro\Model\UsuarioTable;

class EmpresaForm extends Form
{
	protected $usuarioTable;
	
    public function __construct(UsuarioTable $usuarioTable)
    {
        // we want to ignore the name passed
        parent::__construct('empresa');
        
        $this->setUsuarioTable($usuarioTable);
        
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');
        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));
        
        $this->add(array(
        		'name' => 'ciudades_id',
        		'type' => 'Hidden',
        		'options' => array(
        				'value' => '1',
        		),
        ));
        
        $this->add(array(
            'name' => 'nit',
            'type' => 'Text',
            'options' => array(
                'label' => 'Nit',
            ),
        ));
        
        $this->add(array(
            'name' => 'razon_social',
            'type' => 'Text',
            'options' => array(
                'label' => 'Razon social',
            ),
        ));
 
        $this->add(array(
        		'type' => 'Select',
        		'name' => 'contacto_id',
        		'options' => array(
        				'label' => 'Facultad:',
        				'disable_inarray_validator' => false,
        				'value_options' => $this->getUsuarios(),
        		)
        ));
        
        $this->add(array(
        		'name' => 'razon_comercial',
        		'type' => 'Text',
        		'options' => array(
        				'label' => 'Razon comercial',
        		),
        ));
        
        $this->add(array(
        		'name' => 'direccion',
        		'type' => 'Text',
        		'options' => array(
        				'label' => 'Direccion',
        		),
        ));
        
        $this->add(array(
        		'name' => 'telefono',
        		'type' => 'Text',
        		'options' => array(
        				'label' => 'Telefono',
        		),
        ));
        
        $this->add(array(
        		'type' => 'Zend\Form\Element\Csrf',
        		'name' => 'csrf',
        		'options' => array(
        				'csrf_options' => array(
        						'timeout' => 600
        				)
        		)
        ));
        
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Enviar',
            	'class' => 'btn',	
                'id' => 'submitbutton',
            ),
        ));
    }
    
    public function getUsuarios()
    {
    	$table = $this->getUsuarioTable();
    	$data  = $table->fetchAll();
    	$selectData = array();
    
    	foreach ($data as $selectOption) {
    		$selectData[$selectOption->id] = $selectOption->nombres . " " . $selectOption->apellidos;
    	}
    	return $selectData;
    }
    
    public function setUsuarioTable($usuarioTable)
    {
    	$this->usuarioTable = $usuarioTable;
    	return $this;
    }
    
    public function getUsuarioTable()
    {
    	return $this->usuarioTable;
    }
}