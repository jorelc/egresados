<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Registro\Controller\Usuario' => 'Registro\Controller\UsuarioController',
        	'Registro\Controller\Empresa' => 'Registro\Controller\EmpresaController',
        	'Registro\Controller\Oferta' => 'Registro\Controller\OfertaController',
        	'Registro\Controller\Programa' => 'Registro\Controller\ProgramaController',
        	'Registro\Controller\Facultad' => 'Registro\Controller\FacultadController',
        ),
    ),
	// The following section is new and should be added to your file
	'router' => array(
		'routes' => array(
			'usuario' => array(
				'type'    => 'segment',
				'options' => array(
					'route'    => '/usuario[/][:action][/:id]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id'     => '[0-9]+',
					),
					'defaults' => array(
						'controller' => 'Registro\Controller\Usuario',
						'action'     => 'index',
					),
				),
			),
			'programa' => array(
				'type'    => 'segment',
				'options' => array(
					'route'    => '/programa[/][:action][/:id]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id'     => '[0-9]+',
					),
					'defaults' => array(
						'controller' => 'Registro\Controller\Programa',
						'action'     => 'index',
					),
				),
			),
			'facultad' => array(
					'type'    => 'segment',
					'options' => array(
							'route'    => '/facultad[/][:action][/:id]',
							'constraints' => array(
									'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
									'id'     => '[0-9]+',
							),
							'defaults' => array(
									'controller' => 'Registro\Controller\Facultad',
									'action'     => 'index',
							),
					),
			),
			'empresa' => array(
				'type'    => 'segment',
				'options' => array(
					'route'    => '/empresa[/][:action][/:id]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id'     => '[0-9]+',
					),
					'defaults' => array(
						'controller' => 'Registro\Controller\Empresa',
						'action'     => 'index',
					),
				),
			),
			'oferta' => array(
				'type'    => 'segment',
				'options' => array(
					'route'    => '/oferta[/][:action][/:id]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id'     => '[0-9]+',
					),
					'defaults' => array(
						'controller' => 'Registro\Controller\Oferta',
						'action'     => 'index',
					),
				),
			),
		),
	),
    'view_manager' => array(
        'template_path_stack' => array(
            'registro' => __DIR__ . '/../view',
        ),
    ),
);